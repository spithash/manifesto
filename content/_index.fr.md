*L'espace représente le future de l'humanité.*

C'est l'opportunité pour l'humanité d'explorer, développer, utiliser et prospérer différemment. Une manière d'assurer la *longévité, durabilité, ouverture et égalité* de ces efforts au bénéfice de toute l'humanité.

Pour cela nous nous engageons à adhérer à ce qui suit :

Principes
----

1. Toutes les personnes ont le droit d'explorer et d'utiliser l'espace pour le bénéfice et l'intérêt de toute l'humanité.

2. L'exploration et l'utilisation de l'espace doit se faire de manière coopérative et collaborative.

3. L'espace doit être utilisé exclusivement à des fins pacifiques. 

4. Le profit financier ne doit pas être la raison de l'exploration spatiale. 

Toutes les personnes doivent avoir accès aux technologies spatiales et aux donnés qui les accompagnent

***

Pour réaliser ces principes, nous devons adhérer à ce qui suit :

Piliers
----

1.  **Open Source**

   Toutes les technologies développés pour l'espace doivent être publiés enregistrés sous des licenses Open-source 

2.  **Open Data**

   Toute donnée liée ou produite dans l'espace doit être librement accessible, utilisable et élaborée par tout un chacun et en tout lieu, et doit être partagée et managée en accord avec les principes ci-dessus.

3.  **Open Development**

    Toutes les technologies spatiales doivent être développées de manière transparente, lisible, documentée, testable, modulaire et efficace.

4.  **Open Governance**

    Toutes les technologies spatiales doivent être régies de manière participative, collaborative, directe et distribuée.


***


Support
----

The Manifesto is a bedrock for development in the space age. By adding you or your organization, you can show your support for its principles and pillars.

To join as an organization or individual, sign below.

{{< form-sign action="https://airform.io/manifesto@libre.space" >}}